LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY bit_to_hex IS
	PORT(
		CLK_100		:	IN STD_LOGIC;
		input			:	IN STD_LOGIC_VECTOR(3 DOWNTO 0);		
		HEX			:	OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
	);
END bit_to_hex;

ARCHITECTURE behavior OF bit_to_hex IS
BEGIN
	-- Seven segment for address and read/write data
	PROCESS(CLK_100, input)
	BEGIN
		IF rising_edge(CLK_100) THEN
			CASE input IS
			WHEN "0000"=> HEX <="1000000";  -- '0'
			WHEN "0001"=> HEX <="1111001";  -- '1'
			WHEN "0010"=> HEX <="0100100";  -- '2'
			WHEN "0011"=> HEX <="0110000";  -- '3'
			WHEN "0100"=> HEX <="0011001";  -- '4'
			WHEN "0101"=> HEX <="0010010";  -- '5'
			WHEN "0110"=> HEX <="0000010";  -- '6'
			WHEN "0111"=> HEX <="1111000";  -- '7'
			WHEN "1000"=> HEX <="0000000";  -- '8'
			WHEN "1001"=> HEX <="0010000";  -- '9'
			WHEN "1010"=> HEX <="0001000";  -- 'a'
			WHEN "1011"=> HEX <="0000011";  -- 'b'
			WHEN "1100"=> HEX <="1000110";  -- 'c'
			WHEN "1101"=> HEX <="0100001";  -- 'd'
			WHEN "1110"=> HEX <="0000110";  -- 'e'
			WHEN "1111"=> HEX <="0001110";  -- 'f'
			END CASE;
		END IF;
	END PROCESS;

END behavior;