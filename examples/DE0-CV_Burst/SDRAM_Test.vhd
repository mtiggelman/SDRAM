LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY SDRAM_Test IS
	PORT(
		CLK_50		:	IN STD_LOGIC;
		KEY0, KEY3	:	IN STD_LOGIC;
		KEY2			:	IN STD_LOGIC;
		DATA_SW		:	IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		ADDR_SW		:	IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		
		LEDR0, LEDR1:	OUT STD_LOGIC;
		LEDR9, LEDR8:	OUT STD_LOGIC;
		
		HEX0, HEX1	:	OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		HEX2, HEX3	:	OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		HEX4, HEX5	:	OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
		
		-- SDRAM connections
		SDRAM_CLK	:	OUT STD_LOGIC;
		SDRAM_CKE	:	OUT STD_LOGIC;		-- SDRAM ClocK Enable
		SDRAM_CS		:	OUT STD_LOGIC;		-- SDRAM Chip Select
		SDRAM_RAS	:	OUT STD_LOGIC;		-- SDRAM Row Address Strobe command
		SDRAM_CAS	:	OUT STD_LOGIC;		-- SDRAM Column Address Strobe command
		SDRAM_WE		:	OUT STD_LOGIC;		-- SDRAM Write Enable
		
		SDRAM_DQML	:	OUT STD_LOGIC;		-- SDRAM enable lower byte Data IO if low
		SDRAM_DQMH	:	OUT STD_LOGIC;		-- SDRAM enable upper byte Data IO if low
		
		SDRAM_ADDR	:	OUT STD_LOGIC_VECTOR(12 DOWNTO 0);	-- SDRAM Row/Column address 
		SDRAM_BANK	:	OUT STD_LOGIC_VECTOR(1 DOWNTO 0);	-- SDRAM Bank select address
		SDRAM_DQ		:	INOUT STD_LOGIC_VECTOR(15 DOWNTO 0)	-- SDRAM Data IO
	);
END SDRAM_Test;

ARCHITECTURE behavior OF SDRAM_Test IS
	---- SDRAM Controller declaration ----
	COMPONENT SDRAM_Controller_Burst IS
	GENERIC(
		READ_BURST_SIZE : INTEGER := 2;
		WRITE_BURST_SIZE : INTEGER := 2
	);
	PORT(
		-- Controller inputs
		CLK_100		:	IN STD_LOGIC;		-- Controller clock at 100 MHz
		reset			: 	IN STD_LOGIC;		-- Reset active high
		
		-- User connections
		address		:	IN STD_LOGIC_VECTOR(24 DOWNTO 0);
		wr_data		:	IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		rd_data		:	OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		
		read_en		:	IN STD_LOGIC;
		write_en		:	IN STD_LOGIC;
		
		read_rq		:	OUT STD_LOGIC;
		write_rq		:	OUT STD_LOGIC;
		
		busy			:	OUT STD_LOGIC;
		
		-- SDRAM connections
		SDRAM_CKE	:	OUT STD_LOGIC;		-- SDRAM ClocK Enable
		SDRAM_CS		:	OUT STD_LOGIC;		-- SDRAM Chip Select
		SDRAM_RAS	:	OUT STD_LOGIC;		-- SDRAM Row Address Strobe command
		SDRAM_CAS	:	OUT STD_LOGIC;		-- SDRAM Column Address Strobe command
		SDRAM_WE		:	OUT STD_LOGIC;		-- SDRAM Write Enable
		
		SDRAM_DQML	:	OUT STD_LOGIC;		-- SDRAM enable lower byte Data IO if low
		SDRAM_DQMH	:	OUT STD_LOGIC;		-- SDRAM enable upper byte Data IO if low
		
		SDRAM_ADDR	:	OUT STD_LOGIC_VECTOR(12 DOWNTO 0);	-- SDRAM Row/Column address 
		SDRAM_BANK	:	OUT STD_LOGIC_VECTOR(1 DOWNTO 0);	-- SDRAM Bank select address
		SDRAM_DQ		:	INOUT STD_LOGIC_VECTOR(15 DOWNTO 0)	-- SDRAM Data IO
	);
	END COMPONENT;
	
	COMPONENT PLL IS
	PORT (
		refclk   : IN  std_logic := '0'; --  	refclk.clk
		rst      : IN  std_logic := '0'; --   	reset.reset
		outclk_0 : OUT std_logic;        -- 	outclk0.clk
		outclk_1 : OUT std_logic;        -- 	outclk1.clk (180 degree out of phase with clk_0)
		locked   : OUT std_logic         --  	locked.export
	);
	END COMPONENT;
	
	COMPONENT bit_to_hex IS
	PORT(
		CLK_100		:	IN STD_LOGIC;
		input			:	IN STD_LOGIC_VECTOR(3 DOWNTO 0);		
		HEX			:	OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
	);
	END COMPONENT;
	
	SIGNAL reset	: STD_LOGIC := '1';
	SIGNAL CLK_100	: STD_LOGIC;
	
	SIGNAL address	: STD_LOGIC_VECTOR(24 DOWNTO 0);
	SIGNAL wr_data : STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL wr_data2: STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL rd_data	: STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL rd_data_lwr : STD_LOGIC_VECTOR(3 DOWNTO 0);
	
	SIGNAL data_to_sdram : STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL data_from_sdram : STD_LOGIC_VECTOR(31 DOWNTO 0);
	
	SIGNAL rd_data_SDRAM_rq : STD_LOGIC;
	SIGNAL wr_data_SDRAM_rq : STD_LOGIC;
	
	SIGNAL read_rq, write_rq : STD_LOGIC := '0';
	SIGNAL busy 	: STD_LOGIC;
	
	-- States for read/write key presses
	TYPE state_type IS (	WAIT_ST, WRITE_KEY_ST, READ_KEY_ST, WRITE_ST, READ_ST, READ_WAIT_ST, WRITE_WAIT_ST);
	-- Start at INIT_WAIT state
	SIGNAL state: state_type := WAIT_ST;
BEGIN
	-- For now keep these hex displays dark
	HEX2 <= "1111111";
	HEX3 <= "1111111";
	
	-- Address at 0
	address(24 DOWNTO 0) <= (OTHERS => '0');
	--address(3 DOWNTO 0) <= ADDR_SW;
	wr_data(15 DOWNTO 4) <= (OTHERS => '0');
	wr_data(3 DOWNTO 0) <= DATA_SW;
	wr_data2(15 DOWNTO 4) <= (OTHERS => '0');
	wr_data2(3 DOWNTO 0) <= ADDR_SW;
	
	-- LED 8 to busy, if problem might help
	LEDR8 <= busy;
	-- LED0 goes on for writing (KEY0) and LED1 for reading (KEY3)
	LEDR0 <= NOT KEY0;
	LEDR1 <= NOT KEY3;
	
	-- State machine to handle key presses
	PROCESS(CLK_100, reset, KEY2)
		VARIABLE been_high : STD_LOGIC := '0';
	BEGIN
		IF reset='1' or KEY2 = '0' THEN
			read_rq <= '0';
			write_rq <= '0';
			state <= WAIT_ST;
			been_high := '0';
		ELSIF rising_edge(CLK_100) THEN
			CASE state IS
				WHEN WAIT_ST =>
					data_to_sdram <= wr_data;
					been_high := '0';
					IF KEY0 = '0' THEN
						state <= WRITE_KEY_ST;
					ELSIF KEY3 = '0' THEN
						state <= READ_KEY_ST;
					END IF;
				
				WHEN WRITE_KEY_ST =>
					IF busy = '0' THEN
						write_rq <= '1';
						state <= WRITE_ST;
					END IF;
					
				WHEN READ_KEY_ST =>
					IF busy = '0' THEN
						read_rq <= '1';
						state <= READ_ST;
					END IF;
					
				WHEN WRITE_ST =>
					write_rq <= '0';
					IF wr_data_SDRAM_rq = '0' and been_high = '1' THEN
						state <= WRITE_WAIT_ST;
					ELSIF wr_data_SDRAM_rq = '1' THEN
						been_high := '1';
						data_to_sdram <= wr_data2;						
					END IF;					
				
				WHEN READ_ST =>
					read_rq <= '0';
					IF rd_data_SDRAM_rq = '0' and been_high = '1' THEN
						state <= READ_WAIT_ST;
					ELSIF rd_data_SDRAM_rq = '1' THEN
						been_high := '1';
						data_from_sdram(31 DOWNTO 16) <= data_from_sdram(15 DOWNTO 0);
						data_from_sdram(15 DOWNTO 0) <= rd_data;
					END IF;
				
				WHEN READ_WAIT_ST =>
					read_rq <= '0';
					been_high := '0';
					IF KEY3 = '1' THEN
						state <= WAIT_ST;
					END IF;
				
				WHEN WRITE_WAIT_ST =>
					write_rq <= '0';
					been_high := '0';
					IF KEY0 = '1' THEN
						state <= WAIT_ST;
					END IF;
					
				WHEN OTHERS =>
					state <= WAIT_ST;
			END CASE;
		END IF;
	END PROCESS;
	
	-- Init the SDRAM controller
	DUT: SDRAM_Controller_Burst
	PORT MAP(
		CLK_100 => CLK_100,
		reset => reset,
		address => address,
		wr_data => data_to_sdram,
		rd_data => rd_data,
		read_en => read_rq,
		write_en => write_rq,
		read_rq => rd_data_SDRAM_rq,
		write_rq => wr_data_SDRAM_rq,
		busy => busy,
		SDRAM_CKE => SDRAM_CKE,
		SDRAM_CS => SDRAM_CS,
		SDRAM_RAS => SDRAM_RAS,
		SDRAM_CAS => SDRAM_CAS,
		SDRAM_WE => SDRAM_WE,
		SDRAM_DQML => SDRAM_DQML,
		SDRAM_DQMH => SDRAM_DQMH,
		SDRAM_ADDR => SDRAM_ADDR,
		SDRAM_BANK => SDRAM_BANK,
		SDRAM_DQ => SDRAM_DQ
	);
	
	-- Seven segment for address and read/write data
	DATAHEX : bit_to_hex
	PORT MAP(
		CLK_100 => CLK_100,
		input => DATA_SW,
		HEX => HEX0
	);
	
	DATAHEX2 : bit_to_hex
	PORT MAP(
		CLK_100 => CLK_100,
		input => ADDR_SW,
		HEX => HEX1
	);
	
	RDHEX : bit_to_hex
	PORT MAP(
		CLK_100 => CLK_100,
		input => data_from_sdram(19 DOWNTO 16),
		HEX => HEX4
	);	
	
	RDHEX2: bit_to_hex
	PORT MAP(
		CLK_100 => CLK_100,
		input => data_from_sdram(3 DOWNTO 0),
		HEX => HEX5
	);	
	
	-- Reset process, makes sure everything gets reset at boot up. Both the PLL and the SDRAM controller
	PROCESS(CLK_50)
		VARIABLE RESET_COUNTER: UNSIGNED(1 DOWNTO 0) := to_unsigned(3, 2);
	BEGIN
		IF rising_edge(CLK_50) THEN
			IF RESET_COUNTER = to_unsigned(0, 2) THEN
				reset <= '0';
			ELSE
				reset <= '1';
				RESET_COUNTER := RESET_COUNTER - 1;
			END IF;
		END IF;	
	END PROCESS;
	
		-- PLL for generating 100 MHz. THe clock to SDRAM is 180 degrees shifted.
	PLL1: PLL
	PORT MAP(
		refclk => CLK_50,
		rst => reset,
		outclk_0 => CLK_100,
		outclk_1 => SDRAM_CLK,
		locked => LEDR9
	);

END behavior;