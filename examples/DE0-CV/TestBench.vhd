LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY TestBench IS
END ENTITY;

ARCHITECTURE TestBench OF TestBench IS
	---- DUT declaration ----
	COMPONENT SDRAM_Controller IS
	PORT(
		-- Controller inputs
		CLK_100		:	IN STD_LOGIC;		-- Controller clock at 100 MHz
		reset			: 	IN STD_LOGIC;		-- Reset active high
		
		-- User connections
		address		:	IN STD_LOGIC_VECTOR(24 DOWNTO 0);
		wr_data		:	IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		rd_data		:	OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		
		read_en		:	IN STD_LOGIC;
		write_en		:	IN STD_LOGIC;
		
		busy			:	OUT STD_LOGIC;
		
		-- SDRAM connections
		SDRAM_CKE	:	OUT STD_LOGIC;		-- SDRAM ClocK Enable
		SDRAM_CS		:	OUT STD_LOGIC;		-- SDRAM Chip Select
		SDRAM_RAS	:	OUT STD_LOGIC;		-- SDRAM Row Address Strobe command
		SDRAM_CAS	:	OUT STD_LOGIC;		-- SDRAM Column Address Strobe command
		SDRAM_WE		:	OUT STD_LOGIC;		-- SDRAM Write Enable
		
		SDRAM_DQML	:	OUT STD_LOGIC;		-- SDRAM enable lower byte Data IO if low
		SDRAM_DQMH	:	OUT STD_LOGIC;		-- SDRAM enable upper byte Data IO if low
		
		SDRAM_ADDR	:	OUT STD_LOGIC_VECTOR(12 DOWNTO 0);	-- SDRAM Row/Column address 
		SDRAM_BANK	:	OUT STD_LOGIC_VECTOR(1 DOWNTO 0);	-- SDRAM Bank select address
		SDRAM_DQ		:	INOUT STD_LOGIC_VECTOR(15 DOWNTO 0)	-- SDRAM Data IO
	);
	END COMPONENT;

	---- Signal declaration ----
	SIGNAL clk: STD_LOGIC := '0';
	SIGNAL SDRAM_CLK: STD_LOGIC;
	SIGNAL reset: STD_LOGIC := '1';

	SIGNAL CKE: STD_LOGIC;
	SIGNAL CS: STD_LOGIC;
	SIGNAL RAS: STD_LOGIC;
	SIGNAL CAS: STD_LOGIC;
	SIGNAL WE: STD_LOGIC;
	
	SIGNAL address: STD_LOGIC_VECTOR(24 DOWNTO 0);
	SIGNAL wr_data, rd_data: STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL read_en, write_en, busy: STD_LOGIC;
	
	SIGNAL DQML: STD_LOGIC;
	SIGNAL DQMH: STD_LOGIC;
	SIGNAL ADDR: STD_LOGIC_VECTOR(12 DOWNTO 0);
	SIGNAL BANK: STD_LOGIC_VECTOR(1 DOWNTO 0);
	SIGNAL DQ: STD_LOGIC_VECTOR(15 DOWNTO 0);
BEGIN
	---- Stimuli declaration ----
	clk <= NOT clk AFTER 5ns;
	SDRAM_CLK <= NOT clk;
	reset <= '1', '0' AFTER 40ns;
	
	write_en <=	'0',
					'1' AFTER 1000ns,
					'0' AFTER 1010ns;
	read_en <= 	'0',
					'1' AFTER 1130ns,
					'0' AFTER 1140ns;
	address <= "0100000000001110000000001";
	wr_data <= "0101010101010101";
	DQ <= "1111000011110000" AFTER 1110ns;

	---- DUT instantiation ----
	DUT: SDRAM_COntroller
	PORT MAP(
		CLK_100 => clk,
		reset => reset,
		address => address,
		wr_data => wr_data,
		rd_data => rd_data,
		read_en => read_en,
		write_en => write_en,
		busy => busy,
		SDRAM_CKE => CKE,
		SDRAM_CS => CS,
		SDRAM_RAS => RAS,
		SDRAM_CAS => CAS,
		SDRAM_WE => WE,
		SDRAM_DQML => DQML,
		SDRAM_DQMH => DQMH,
		SDRAM_ADDR => ADDR,
		SDRAM_BANK => BANK,
		SDRAM_DQ => DQ
	);

END ARCHITECTURE;