-------------------------------------------------------------------------------
-- SDRAM Controller with burst
-- A simple SDRAM controller implementation: setable burst length, random access
-------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

ENTITY SDRAM_Controller_Burst IS
	GENERIC(
		READ_BURST_SIZE : INTEGER := 2;
		WRITE_BURST_SIZE : INTEGER := 2
	);
	PORT(
		-- Controller inputs
		CLK_100		:	IN STD_LOGIC;		-- Controller clock at 100 MHz
		reset			: 	IN STD_LOGIC;		-- Reset active high
		
		-- User connections
		address		:	IN STD_LOGIC_VECTOR(24 DOWNTO 0);
		wr_data		:	IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		rd_data		:	OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		
		-- Read/write commands
		read_en		:	IN STD_LOGIC;
		write_en		:	IN STD_LOGIC;
		-- Data requests from the SDRAM controller that new data is needed
		read_rq		:	OUT STD_LOGIC;
		write_rq		:	OUT STD_LOGIC;
		-- High if controller is busy
		busy			:	OUT STD_LOGIC;
		
		-- SDRAM connections
		SDRAM_CKE	:	OUT STD_LOGIC;		-- SDRAM ClocK Enable
		SDRAM_CS		:	OUT STD_LOGIC;		-- SDRAM Chip Select
		SDRAM_RAS	:	OUT STD_LOGIC;		-- SDRAM Row Address Strobe command
		SDRAM_CAS	:	OUT STD_LOGIC;		-- SDRAM Column Address Strobe command
		SDRAM_WE		:	OUT STD_LOGIC;		-- SDRAM Write Enable
		
		SDRAM_DQML	:	OUT STD_LOGIC;		-- SDRAM enable lower byte Data IO if low
		SDRAM_DQMH	:	OUT STD_LOGIC;		-- SDRAM enable upper byte Data IO if low
		
		SDRAM_ADDR	:	OUT STD_LOGIC_VECTOR(12 DOWNTO 0);	-- SDRAM Row/Column address 
		SDRAM_BANK	:	OUT STD_LOGIC_VECTOR(1 DOWNTO 0);	-- SDRAM Bank select address
		SDRAM_DQ		:	INOUT STD_LOGIC_VECTOR(15 DOWNTO 0)	-- SDRAM Data IO
	);
END SDRAM_Controller_Burst;

ARCHITECTURE behavior OF SDRAM_Controller_Burst IS
	-- Define SDRAM controller states
	TYPE state_type IS (	INIT_WAIT_ST, INIT_PRECHARGE_ST, INIT_REFRESH_ST, INIT_SETMODE_ST, IDLE_ST,
								WRITE_ST, READ_ST, READ_STOP_ST, REFRESH_ST, PRECHARGE_ST);
	-- Start at INIT_WAIT state
	SIGNAL state: state_type := INIT_WAIT_ST;
	-- Use for simulation purposes
	SIGNAL state_up: state_type;
	
	-- All the used commands classified under constants
	-- These commands consists of: CS, RAS, CAS, WE
	SUBTYPE cmd_type is STD_LOGIC_VECTOR(3 DOWNTO 0);
	CONSTANT CMD_MODE			: cmd_type := "0000";
	CONSTANT CMD_REFRESH		: cmd_type := "0001";
	CONSTANT CMD_PRECHARGE	: cmd_type := "0010";
	CONSTANT CMD_ACTIVATE	: cmd_type := "0011";
	CONSTANT CMD_WRITE		: cmd_type := "0100";
	CONSTANT CMD_READ			: cmd_type := "0101";
	CONSTANT CMD_BST			: cmd_type := "0110";
	CONSTANT CMD_NOP			: cmd_type := "0111";
	
	SIGNAL SDRAM_CMDb : cmd_type;
	
	-- Mode configuration: set to full page burst and CAS latency = 2
	CONSTANT MODE_CONF : STD_LOGIC_VECTOR(12 DOWNTO 0) := "0000000100111";
	
	-- Internal signals to buffer output
	SIGNAL ADDRb : STD_LOGIC_VECTOR(12 DOWNTO 0);
	SIGNAL BANKb : STD_LOGIC_VECTOR(1 DOWNTO 0);
	SIGNAL DATAb : STD_LOGIC_VECTOR(15 DOWNTO 0);
	SIGNAL write_rqb : STD_LOGIC;
	
	-- Used to set direction of SDRAM DQ IO
	SIGNAL DQdir, DQdirb	: STD_LOGIC;
	
	-- Refresh signals
	SIGNAL refresh, refresh_reset : STD_LOGIC;
	
	-- Read process enable
	SIGNAL enable_read : STD_LOGIC;
	
	-- Internal signals
	SIGNAL bank_addr 	: STD_LOGIC_VECTOR(1 DOWNTO 0);
	SIGNAL row_addr	: STD_LOGIC_VECTOR(12 DOWNTO 0);
	SIGNAL col_addr	: STD_LOGIC_VECTOR(9 DOWNTO 0);

BEGIN
	-- Set SDRAM_DQML/H active (low)
	SDRAM_DQML <= '0';
	SDRAM_DQMH <= '0';
	-- Always enable clock
	SDRAM_CKE <= '1';
	-- Select as output or input
	SDRAM_DQ  <= wr_data WHEN (DQdir = '1') ELSE (OTHERS => 'Z');
	
	-- Decode address to bank-row-column
	bank_addr <= address(24 DOWNTO 23);
	row_addr <= address(22 DOWNTO 10);
	col_addr <= address(9 DOWNTO 0);
	
	-- Process to read set amount of data
	PROCESS(CLK_100, reset, enable_read)
		VARIABLE WAIT_COUNTER: INTEGER RANGE 0 TO 3:= 3;
	BEGIN
		IF enable_read = '0' THEN
			WAIT_COUNTER := 3;
			read_rq <= '0';
		ELSIF rising_edge(CLK_100) THEN
			IF WAIT_COUNTER = 0 THEN
				rd_data <= SDRAM_DQ;
				read_rq <= '1';
			ELSE
				WAIT_COUNTER := WAIT_COUNTER-1;
				read_rq <= '0';
			END IF;
		END IF;	
	END PROCESS;
	
	-- State machine process
	PROCESS(CLK_100, reset)
		-- A variable to stall going to the next state: wait until certain time has passed
		VARIABLE WAIT_COUNTER: UNSIGNED(15 DOWNTO 0) := to_unsigned(0, 16);
		-- A variable for counting the number of refresh cycles still remaining -> SDRAM init requirement
		VARIABLE REFR_COUNTER: UNSIGNED(2 DOWNTO 0) := to_unsigned(0, 3);	
		-- Write counter
		VARIABLE WRITE_COUNTER: INTEGER RANGE 0 TO WRITE_BURST_SIZE := 0;
	BEGIN
		-- At reset go to first state to initialise SDRAM
		IF reset = '1' THEN
			state <= INIT_WAIT_ST;
			SDRAM_CMDb <= CMD_NOP;
			DQdirb <= '0';
			busy <= '1';
			enable_read <= '0';
			ADDRb <= (OTHERS => '0');
			BANKb <= (OTHERS => '0');
			DATAb <= (OTHERS => '0');
			WAIT_COUNTER := to_unsigned(0, 16);
			write_rqb <= '0';
		ELSIF rising_edge(CLK_100) THEN
				-- Always reset command to NOP so changes only stay one clock cycle
				SDRAM_CMDb <= CMD_NOP;
				-- Can always be given address, only at MODE SELECT it matters for 1 clk cycle
				BANKb <= bank_addr;
				-- Address bank always at column address unless activating for 1 clk cycle
				ADDRb <= "000" & col_addr;
				-- Keep data lines default as input, unless writing 1 clk cycle
				DQdirb <= '0';			
				
			IF WAIT_COUNTER = to_unsigned(0,16) THEN
				-- State variable just for simulation checking
				--state_up <= state;				
				
				CASE state IS
					-- At start SDRAM requires 200 us of waiting time with NOP command
					WHEN INIT_WAIT_ST =>
						WAIT_COUNTER := to_unsigned(20000, 16);
						--WAIT_COUNTER := to_unsigned(10, 16); -- for simulation only!
						state <= INIT_PRECHARGE_ST;
					
					-- Then precharge all the banks
					WHEN INIT_PRECHARGE_ST =>
						-- Wait two clock cycles after starting the precharge
						WAIT_COUNTER := to_unsigned(2, 16);
						-- Next state do 7 refresh cycles
						REFR_COUNTER := to_unsigned(7, 3);
						-- Precharge all banks (setting address 10 high)
						SDRAM_CMDb <= CMD_PRECHARGE;
						ADDRb(10) <= '1';
						state <= INIT_REFRESH_ST;
					
					-- Run a minimum of 8 refresh cycles
					WHEN INIT_REFRESH_ST =>
						-- Once finished move on to next state
						IF REFR_COUNTER = to_unsigned(0, 3) THEN
							state <= INIT_SETMODE_ST;
						ELSE
							-- Every refresh cycle takes 6 clock cycles
							WAIT_COUNTER := to_unsigned(8, 16);
							REFR_COUNTER := REFR_COUNTER - 1;
							SDRAM_CMDb <= CMD_REFRESH;
						END IF;
					
					-- This sets the CAS mode and the burst size of the SDRAM
					WHEN INIT_SETMODE_ST =>
						-- Wait 2 clock cycles until mode has been set
						WAIT_COUNTER := to_unsigned(2, 16);
						-- Set mode command and address to mode config
						SDRAM_CMDb <= CMD_MODE;
						ADDRb <= MODE_CONF;
						BANKb <= "00";
						refresh_reset <= '1';
						state <= IDLE_ST;
					
					-- In IDLE, wait for read or write command or until necessary refresh cycle
					WHEN IDLE_ST =>			
						busy <= '0';
						refresh_reset <= '0';
						WRITE_COUNTER := WRITE_BURST_SIZE;
						enable_read <= '0';
						write_rqb <= '0';
						-- At refresh give refresh command and wait 6 cycles (5 + state change)
						IF refresh = '1' THEN
							SDRAM_CMDb <= CMD_REFRESH;
							state <= REFRESH_ST;
							busy <= '1';
							WAIT_COUNTER := to_unsigned(5, 16);
						-- Before read or writing, activate the correct row and bank. Takes 2 cycles.
						ELSIF read_en = '1'THEN								
							SDRAM_CMDb <= CMD_ACTIVATE;
							ADDRb <= row_addr;
							state <= READ_ST;
							busy <= '1';
							WAIT_COUNTER := to_unsigned(2, 16);
						ELSIF write_en = '1' THEN							
							SDRAM_CMDb <= CMD_ACTIVATE;
							ADDRb <= row_addr;
							state <= WRITE_ST;
							busy <= '1';
							WAIT_COUNTER := to_unsigned(2, 16);
						END IF;
					
					-- Reset refresh counter and go back to IDLE state
					WHEN REFRESH_ST =>
						refresh_reset <= '1';
						state <= IDLE_ST;
					
					-- Give read command
					WHEN READ_ST =>
						SDRAM_CMDb <= CMD_READ;
						state <= READ_STOP_ST;
						enable_read <= '1';
						WAIT_COUNTER := to_unsigned(READ_BURST_SIZE-1,16);
					
					-- Terminate the burst after set amount of data
					WHEN READ_STOP_ST =>
						SDRAM_CMDb <= CMD_BST;
						state <= PRECHARGE_ST;
						WAIT_COUNTER := to_unsigned(2,16);
						
					-- Give the write command and present the write data at the same clock
					WHEN WRITE_ST =>												
						IF WRITE_COUNTER = 0 THEN
							-- When done, go to the precharge state. Tdpl gives 2 clk between input data to precharge command
							state <= PRECHARGE_ST;
							WAIT_COUNTER := to_unsigned(1, 16);
							write_rqb <= '0';
						ELSIF WRITE_COUNTER = WRITE_BURST_SIZE THEN
							SDRAM_CMDb <= CMD_WRITE;
							WRITE_COUNTER := WRITE_COUNTER - 1;
							DQdirb <= '1';
							write_rqb <= '1';
						ELSE
							WRITE_COUNTER := WRITE_COUNTER - 1;
							DQdirb <= '1';
							write_rqb <= '1';
						END IF;
						
						-- Give precharge command and precharge all banks. Afterwards return to IDLE
					WHEN PRECHARGE_ST =>		
						SDRAM_CMDb <= CMD_PRECHARGE;
						ADDRb(10) <= '1';
						state <= IDLE_ST;
					
					WHEN OTHERS =>
						state <= IDLE_ST;
				END CASE;
				
			ELSE				
				WAIT_COUNTER := WAIT_COUNTER - 1;
			END IF;
			
		END IF;
	END PROCESS;
	
	-- Refresh counter process, every 7us give refresh signal
	PROCESS(CLK_100, refresh_reset)
		VARIABLE REFRESH_COUNTER: UNSIGNED(9 DOWNTO 0) := to_unsigned(700, 10);
	BEGIN
		IF refresh_reset = '1' THEN
			REFRESH_COUNTER := to_unsigned(700, 10);
			refresh <= '0';
		ELSIF rising_edge(CLK_100) THEN
			IF REFRESH_COUNTER = to_unsigned(0, 10) THEN
				refresh <= '1';
			ELSE
				refresh <= '0';
				REFRESH_COUNTER := REFRESH_COUNTER - 1;
			END IF;
		END IF;
	END PROCESS;
	
	-- Process to buffer all outgoing signals
	PROCESS(CLK_100)
	BEGIN
		IF rising_edge(CLK_100) THEN
			SDRAM_ADDR	<= ADDRb;
			SDRAM_BANK 	<= BANKb;
			DQdir 		<= DQdirb;
			SDRAM_CS 	<= SDRAM_CMDb(3);
			SDRAM_RAS 	<= SDRAM_CMDb(2);
			SDRAM_CAS	<= SDRAM_CMDb(1);
			SDRAM_WE		<= SDRAM_CMDb(0);
			write_rq		<= write_rqb;
		END IF;
	END PROCESS;

END behavior;