# Basic SDRAM Controller
## Description
A basic SDRAM controller implementation in VHDL. It allows for random acces write
and read operations. Read/write times are 70ns. The controller takes care that
the memory cells get refreshed every 7us. Currently it's configured for a
100 MHz clock and a 2 cycle CAS latency.

At the moment it has only been tested and is optimized for the IS42S16320D: 512
Mb SDRAM with a 16-bit data bus. This is the chip used on the DE0-CV board by
Terasic. An example project is provided in the `examples` folder.

## Structure
The following files can be found in the `source` folder:
* `SDRAM_Controller.vhd`: a basic SDRAM controller implementation without bursts
* `SDRAM_Controller_Burst.vhd`: a slightly more advanced SDRAM controller which allows for burst transfers

The `examples` folder contains examples with the SDRAM controller in use. At the
moment there are only projects for the DE0-CV board which allows the user to test
basic read/write functionality.

## Usage
The following sections will explain how the SDRAM controller should be used from a
user point of view and how it should be wired up with the SDRAM chip.

### SDRAM signals
All signals starting with `SDRAM_x` should be hooked up to the corresponding pins
of the SDRAM chip. In this design, the SDRAM is always activated. So
expect warnings that `SDRAM_CKE, SDRAM_CS, SDRAM_DQML` and `SDRAM_DQMH` are stuck
at GND or VCC. This is by design.

The SDRAM Controller expects that a 100 MHz clock is supplied to the SDRAM chip.
**Important**: this clock should be 180 degrees out of phase (inverted)
compared to the clock internally. If this condition is not met, the data will not be
ready to be sampled at the correct time.

### User signals
These are the signals that the user will have to deal with. Both versions have
slightly different interfacing signals. The most basic version is the one without
burst capability, as only one word can be read or written at a time. Two extra
control signals have been added to the controller with burst capability, to make
sure that data can be presented or saved on time.
#### SDRAM without burst
The following table describes all the user signals for the SDRAM controller without
burst capabilities:

| Signal     | Size     | Desciption  |
| ---------- |:--------:| -----|
| address    | 25-bit   | The address at which the operation should take place |
| wr_data    | 16-bit   | Data to be written to supplied address |
| rd_data    | 16-bit   | The last read data. Updates when busy goes low |
| read_en    | logic    | Starts a read operation at the current address |
| write_en   | logic    | Starts a write operation at the current address |
| busy       | logic    | If high: controller is busy. No new read/write possible |

#### SDRAM with burst
In addition to the signals mentioned above, there are a couple of extra ones for
the controller with burst capability:

| Signal     | Size     | Desciption  |
| ---------- |:--------:| -----|
| read_rq    | logic    | Signal from the SDRAM controller that new read data is available |
| write_rq   | logic    | Request from the SDRAM controller that new data should be presented to be written to the SDRAM |

### Adressing
The address signal is 25-bit wide. Encoded within are the banks, rows and columns
of the SDRAM:

| Bits  24-23 | Bits  22-10       | Bits  9-0 |
| :---------: | :-------------:   | :-------: |
| Bank: B1-B0 | 8192 Rows: A12-A0 | 1024 Columns: A8-A0 |
